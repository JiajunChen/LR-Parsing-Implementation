public class GrammarNode 
{
	private String grammar_Name;
	private GrammarNode next;
	GrammarNode(String name)
	{
		grammar_Name=name;
		next=null;
	}
	public String get_grammar_Name()
	{
		return grammar_Name;
	}
	public void set_grammer_Name(String name)
	{
		grammar_Name=name;
	}
	public GrammarNode getNext()
	{
		return next;
	}
	public void setNext(GrammarNode n)
	{
		next=n;
	}
	public String toString()
	{
		if(next!=null)
			return grammar_Name+ " ";
		else
			return grammar_Name;
	}
}
